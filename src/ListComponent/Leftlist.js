import React from 'react';

function Leftlist(props){
  const leftInputList = props.listArrayItems;
  const DisplayLeftList= leftInputList.map((item,index) => {
    return <li key={index} onClick={ () => props.pushfromLeft(index,item)}>{item}</li>
  });
  return(
    <>
     <ul>{DisplayLeftList}</ul>
    </>
  )
}
export default Leftlist;