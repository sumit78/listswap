import React from 'react';
import Leftlist from './Leftlist';
import RightList from './Rightlist';
import './Style.css';

class AddItem extends React.Component{
  constructor(props){
    super(props)
      this.state={
        inputItemLeftArray: [],
        isDisplay: false,
        inputItemRightArray:[]
      };
  }

  handleClick =() => {
    const inputEle = document.getElementById('inputEle');
    const leftList = this.state.inputItemLeftArray;
    leftList.push(inputEle.value);
    this.setState({
      isDisplay:true,
      inputItemLeftArray: leftList
    });
  }

  pushHandlerLeft = (index,item) => {
    const leftList = this.state.inputItemLeftArray;
    this.state.inputItemRightArray.push(item);
    console.log("index"+index,"item"+item);
    leftList.splice(index,1);
    this.setState({
      inputItemLeftArray: leftList
    });
  }

  pushHandlerRight = (selectedItem2) => {
   alert("world");
  }

  render(){
    return(
      <div>
        <div className="center" >
          <input type="text" id="inputEle"/>
          <button className="button button2" onClick={this.handleClick} type="button">ADD Items</button>
        </div>
        <div className="flex-container">
          <div>
             {this.state.isDisplay &&
           <Leftlist listArrayItems = {this.state.inputItemLeftArray} pushfromLeft = {this.pushHandlerLeft}/>
              }
          </div>
          <div>
            <RightList rightListArrayItems = {this.state.inputItemRightArray} pushfromRight = {this.pushHandlerRight}/>
          </div>
        </div>
      </div>
    )
  }
}

export default AddItem;