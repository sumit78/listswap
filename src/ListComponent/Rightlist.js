import React from 'react'

class Rightlist extends React.Component{

  render(){
    const getArrayList = this.props.rightListArrayItems; 
    const displayList = getArrayList.map((item,index) => {
      return <li key={index} onClick = {this.props.pushfromRight.bind(this, item)}>{item}</li>
    });
    return(
      <>
        <ul>{displayList}</ul>
      </>
    )
  }
}
export default Rightlist;