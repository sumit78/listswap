import React,{useState,useEffect} from 'react';

function Example1 (){
  const [count,setCount]=useState(0);

  useEffect(()=> {
    document.title=`you clicked ${count} times`;
  });
  return(
    <>
    <p> You Clicked {count} times</p>
    <button onClick ={()=> setCount(count+1)}>
      click me
      </button>
    </>
  )
}
export default  Example1;