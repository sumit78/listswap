import React from 'react';

function RightList(props){
  const {list} = props;
  return(
    <>
      {
        list.map((item,index) => (
      <div key={index}>{item.value}</div>
      ))
      }
    </>
  )

}
export default RightList;

