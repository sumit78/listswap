import React,{Component} from 'react';
import LeftList from './Leftlist';
import AddItem from './AddItem';

class ListManager extends Component {
  constructor(props){
    super(props);
      this.state={
        leftListItems:[],
        rightListItems:[],
      }
    }
    handleAddItem = (e,item) =>{
      e.preventDefault();
      const {leftListItems} = this.state;
      leftListItems.push(item);
      this.setState({
        leftListItems
      })

    }

    render(){
      return (
        <>
        <AddItem handleAddItem = {this.handleAddItem}/>
        <LeftList  leftListItems = {this.state.leftListItems}/>
        </>
      )
    }
}

export default ListManager;