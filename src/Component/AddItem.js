import React from 'react';

class AddItem extends React.Component{
  constructor(props){
    super(props)
    this.state={
      inputItem:"",
    }
   
  }
  handleChange = (event) => {
    this.setState({
      inputItem:event.target.value
    });
  }


  
  render(){
    console.log('printing props',this.props);
    return(
      <div>
        <form onSubmit={(e)=>this.props.handleAddItem(e,this.state.inputItem)}>
        <input type="text" value = {this.state.inputItem} onChange = {this.handleChange}></input>
        <button type="submit" >AddItem</button>
        </form>
      </div>
    )
    
  }
}

export default AddItem;