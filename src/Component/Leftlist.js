import React from 'react';

class Leftlist extends React.Component{

  render(){
    const listArray = this.props.leftListItems;
    console.log(listArray);
    const displayLeftList= listArray.map((item,index) => {
       return <li key={index}> {item}</li> 
    })
    return(
      <div>
         {displayLeftList}
         <button type="button" onClick={this.props.handleLeft}> left </button>
        </div>
    )
  }
}
export default Leftlist;