import {Suspense,lazy} from 'react';
import React from 'react';

const  Routing = lazy(() => import('./Bookshop/Routing'))
//const Books = lazy(() => import('./Bookshop/Books'));

function App() {
  return (
    <div>
      <Suspense fallback = {<div>Loading....</div>}>
       <Routing/>
      </Suspense>
    </div>
  );
}

export default App;
