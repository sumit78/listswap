export default {
music:['A Visit from the Goon Squad','No Walls and the Recurring Dream',
'Talking to Girls About Duran Duran','Porcelain'],
kids:['Dont Let the Pigeon Drive the Bus','Goodnight',
'Construction Site','Goodnight Moon','The Snowy Day'],
cooking:['Honey & Co: At Home','Zaitoun Recipes and Stories from the Palestinian Kitchen',
'Berber & Q by Josh Katz','Jamie Cooks Italy by Jamie Oliver','Venice','A Table in Venice'],
education:['Official Knowledge: Democratic Education in a Conservative Age ','Between Past and Future','Culture and Anarchy ',
'Giving Teaching Back to the Teachers','Learning Beyond The Classroom: Education for a Changing World'],
computer:['Introduction to Computer Science Using Python','Code: The Hidden Language of Computer Hardware and Software','JavaScript The Good Parts','The Pragmatic Programmer']
}

  
