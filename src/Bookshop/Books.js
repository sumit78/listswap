import {Link} from 'react-router-dom';
import React from 'react';

 function Books(){
    return(
      <>
          <div>
            <ul>
              <li>
                <Link to="/music">Music</Link>
              </li>
              <li>
                <Link to="/kids">Kids</Link>
              </li>
              <li>
                <Link to="/cooking">Cooking</Link>
              </li>
              <li>
                <Link to="/edu">Education</Link>
              </li>
              <li>
                <Link to="/computers">Computers</Link>
              </li>
            </ul>
            <hr/>
          </div>
      </>
    )
  }
 
export default Books;