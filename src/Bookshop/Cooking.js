import React from 'react';
import booksData from './Store';

const Cooking = () => {
  let booksDisplay = booksData.cooking.map((item,index)=>{
    return <li key = {index} onClick={()=>{console.log(item)}}>{item} </li>
  });
  return(
    <ol>
      <h2>{booksDisplay}</h2>     
    </ol>
  )
};

export default Cooking;