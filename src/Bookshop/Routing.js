import React from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom';

import Books from './Books';
import Music from './Music';
import Kids from './Kids';
import Education from './Education';
import Cooking from './Cooking';
import Computers from './Computers';

const Routing = () => {
  return(
    <div>
      <Router>
        <Route path="/" component={Books}></Route> 
        <Route path="/music" component={Music} ></Route>
        <Route path="/kids" component={Kids}></Route>
        <Route path="/cooking" component={Cooking}></Route>
        <Route path="/edu" component={Education}></Route>
        <Route path="/computers" component={Computers}></Route>
      </Router>
    </div>
  )
}

export default Routing;

   